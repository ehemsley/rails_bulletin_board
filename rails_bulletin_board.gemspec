$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rails_bulletin_board/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rails_bulletin_board"
  s.version     = RailsBulletinBoard::VERSION
  s.authors     = ["Evan Hemsley"]
  s.email       = ["evan.hemsley@gmail.com"]
  s.homepage    = "TODO"
  s.summary     = "A mountable Rails bulletin board engine"
  s.description = "A mountable Rails engine that adds bulletin board functionality to your website"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.0.4"
  s.add_dependency "rspec-rails", "~> 2.14.1"
  s.add_dependency "factory_girl_rails", "~> 4.4.1"
  s.add_dependency "devise"
  s.add_dependency "slim"
  s.add_dependency "rolify"
  s.add_dependency "authority"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "database_cleaner"
end
