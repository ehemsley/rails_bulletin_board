require_dependency "rails_bulletin_board/application_controller"

module RailsBulletinBoard
  class ThreadController < ApplicationController
    before_filter :authenticate_user!
    before_filter :build_thread, only: [:new, :create]
    before_filter :get_thread, only: [:edit, :show, :update]

    def create
      if @thread.update_attributes(thread_params.merge({ author_id: current_user.id }))
        redirect_to @thread
      else
        render :new
      end
    end

    def edit
    end

    def new
    end

    def show
      @thread.update_attributes(view_count: @thread.view_count + 1)
    end

    def update
      @thread.update_attributes(thread_params)
    end

    private

    def build_thread
      @thread = RailsBulletinBoard::Thread.new
    end

    def get_thread
      @thread = RailsBulletinBoard::Thread.find(params[:id])
    end

    def thread_params
      params.require(:thread).permit(:author_id, :name, posts_attributes: [:content] )
    end
  end
end
