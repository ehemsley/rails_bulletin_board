require_dependency "rails_bulletin_board/application_controller"

module RailsBulletinBoard
  class PostController < ApplicationController
    before_filter :authenticate_user!
    before_filter :build_post, only: [:new, :create]
    before_filter :get_post, only: [:edit, :update]

    def create
      if @post.update_attributes(post_params)
        redirect_to @post.thread
      else
        render :new
      end
    end

    def edit
    end

    def new
    end

    def update
      @post.update_attributes(post_params)
    end

    private

    def post_params
      params.require(:post).permit(:content, :thread_id)
    end

    def build_post
      @post = RailsBulletinBoard::Post.new
    end

    def get_post
      @post = RailsBulletinBoard::Post.find(params[:id])
    end
  end
end
