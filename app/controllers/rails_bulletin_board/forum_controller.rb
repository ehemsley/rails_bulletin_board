require_dependency "rails_bulletin_board/application_controller"

module RailsBulletinBoard
  class ForumController < ApplicationController
    before_filter :get_forum, only: :show

    def show
    end

    private

    def forum_params
      params.require(:forum).permit(:name, :description)
    end

    def get_forum
      @forum = RailsBulletinBoard::Forum.find(params[:id])
    end
  end
end
