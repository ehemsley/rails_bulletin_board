module RailsBulletinBoard
  class Forum < ActiveRecord::Base
    include Authority::Abilities

    resourcify

    has_many :threads
  end
end
