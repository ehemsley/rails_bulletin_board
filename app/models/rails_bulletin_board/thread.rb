module RailsBulletinBoard
  class Thread < ActiveRecord::Base
    include Authority::Abilities

    belongs_to :author, class_name: "User", foreign_key: :author_id
    belongs_to :forum
    has_many :posts

    accepts_nested_attributes_for :posts
  end
end
