module RailsBulletinBoard
  class Post < ActiveRecord::Base
    include Authority::Abilities

    belongs_to :thread
    belongs_to :user
  end
end
