module RailsBulletinBoard
  class ThreadAuthorizer < ApplicationAuthorizer
    def creatable_by?(user)
      if resource.forum.private || resource.forum.locked || resource.locked
        moderatable_by?(user)
      else
        true
      end
    end

    def readable_by?(user)
      if resource.forum.private
        moderatable_by?(user)
      else
        true
      end
    end

    def deletable_by?(user)
      moderatable_by?(user)
    end

    private

    def moderatable_by?(user)
      user.has_role?(:admin) ||
        user.has_role?(:moderator, resource.forum)
    end
  end
end
