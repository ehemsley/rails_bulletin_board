module RailsBulletinBoard
  class PostAuthorizer < ApplicationAuthorizer
    def creatable_by?(user)
      if resource.thread.locked || resource.thread.forum.private?
        manageable_by?(user)
      else
        true
      end
    end

    def updatable_by?(user)
      manageable_by?(user)
    end

    def deletable_by?(user)
      manageable_by?(user)
    end

    private

    def manageable_by?(user)
      resource.user_id == user.id ||
        moderatable_by?(user)
    end

    def moderatable_by?(user)
      user.has_role?(:admin) ||
        user.has_role?(:moderator, resource.thread.forum)
    end
  end
end
