require 'devise'
require 'slim'
require 'rolify'
require 'authority'

module RailsBulletinBoard
  class Engine < ::Rails::Engine
    isolate_namespace RailsBulletinBoard

    config.generators do |g|
      g.test_framework :rspec
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
      g.templates.unshift File::expand_path('../templates', __FILE__)
    end
  end
end
