# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :rails_bulletin_board_thread, class: RailsBulletinBoard::Thread do
    name "Dumb Thread"
    association :forum, factory: :rails_bulletin_board_forum
  end
end
