# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :rails_bulletin_board_forum, :class => RailsBulletinBoard::Forum do
    name "Test Forum"
    description "A board for tests"
  end
end
