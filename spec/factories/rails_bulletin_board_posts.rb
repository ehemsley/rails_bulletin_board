# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :rails_bulletin_board_post, :class => 'RailsBulletinBoard::Post' do
    association :thread, factory: :rails_bulletin_board_thread
  end
end
