require 'spec_helper'

module RailsBulletinBoard
  describe PostController do
    let(:thread) { create(:rails_bulletin_board_thread) }
    let(:thread_post) { create(:rails_bulletin_board_post) }
    let(:user) { create(:rails_bulletin_board_user) }

    before do
      sign_in user
    end

    context 'user creates post' do
      it 'redirects to thread' do
        post :create, { post: { thread_id: thread.id, content: 'Bad Post' }, use_route: :posts }
        assert_response :redirect
      end
    end
  end
end
