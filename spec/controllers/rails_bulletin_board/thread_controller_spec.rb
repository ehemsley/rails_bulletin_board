require 'spec_helper'

module RailsBulletinBoard
  describe ThreadController do
    let(:thread) { create(:rails_bulletin_board_thread) }
    let(:user) { create(:rails_bulletin_board_user) }

    before do
      sign_in user
    end

    context 'user views thread' do
      it 'returns 200' do
        get :show, id: thread.id, use_route: :threads
        assert_response :ok
      end

      it 'increments the view count by 1' do
        expect{ get :show, id: thread.id, use_route: :threads }.to change{thread.reload.view_count}.by(1)
      end
    end

    context 'user creates thread' do
      let(:create_thread) { post :create, thread: { name: 'Dumb Thread', forum_id: 1, posts_attributes: [{ content: "Test post" }] }, use_route: :threads }

      it 'returns 200' do
        create_thread
        assert_response :redirect
      end

      it 'creates a new post as the first post in the thread' do
        expect{ create_thread }.to change(Post, :count).by(1)
      end

    end

  end
end
