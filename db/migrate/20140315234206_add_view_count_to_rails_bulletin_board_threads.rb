class AddViewCountToRailsBulletinBoardThreads < ActiveRecord::Migration
  def change
    add_column :rails_bulletin_board_threads, :view_count, :integer, default: 0, null: false
  end
end
