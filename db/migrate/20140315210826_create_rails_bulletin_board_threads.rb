class CreateRailsBulletinBoardThreads < ActiveRecord::Migration
  def change
    create_table :rails_bulletin_board_threads do |t|
      t.string :name
      t.integer :author_id
      t.integer :forum_id

      t.timestamps
    end
  end
end
