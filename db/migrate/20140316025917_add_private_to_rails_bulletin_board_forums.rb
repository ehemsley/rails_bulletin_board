class AddPrivateToRailsBulletinBoardForums < ActiveRecord::Migration
  def change
    add_column :rails_bulletin_board_forums, :private, :boolean, default: false, not_null: true
  end
end
