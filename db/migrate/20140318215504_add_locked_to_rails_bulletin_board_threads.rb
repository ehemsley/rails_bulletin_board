class AddLockedToRailsBulletinBoardThreads < ActiveRecord::Migration
  def change
    add_column :rails_bulletin_board_threads, :locked, :boolean
  end
end
