class CreateRolifyRoles < ActiveRecord::Migration
  def change
    create_table(:rails_bulletin_board_roles) do |t|
      t.string :name
      t.references :resource, :polymorphic => true

      t.timestamps
    end

    create_table(:users_roles, :id => false) do |t|
      t.references :rails_bulletin_board_user
      t.references :rails_bulletin_board_role
    end

    add_index(:rails_bulletin_board_roles, :name)
    add_index(:rails_bulletin_board_roles, [ :name, :resource_type, :resource_id ], unique: true, name: 'role_name_index')
    add_index(:users_roles, [ :rails_bulletin_board_user_id, :rails_bulletin_board_role_id ], unique: true, name: 'user_role_join_index')
  end
end
