class CreateRailsBulletinBoardPosts < ActiveRecord::Migration
  def change
    create_table :rails_bulletin_board_posts do |t|
      t.integer :thread_id
      t.integer :user_id

      t.timestamps
    end
  end
end
