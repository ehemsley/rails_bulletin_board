class AddLockedToRailsBulletinBoardForums < ActiveRecord::Migration
  def change
    add_column :rails_bulletin_board_forums, :locked, :boolean
  end
end
